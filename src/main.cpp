#include <Arduino.h>
#include <stdlib.h>  
#include "esp_camera.h"
#include "esp_timer.h"
#include "fb_gfx.h"
#include "fd_forward.h"
#include "fr_forward.h"
#include "Arduino.h"
#include <WiFi.h>
#include "time.h"
#include <NTPClient.h>
#include <WiFiClient.h>
#include <WiFiType.h>
#include <IPAddress.h>
#include <HardwareSerial.h>
#include <WString.h>
#include "soc/rtc_cntl_reg.h"
#include "soc/soc.h"
#include "img_converters.h"
#include "soc/rtc_cntl_reg.h"
#include "esp_http_server.h"
#include "FS.h"               
#include "SD_MMC.h"            
#include <EEPROM.h>    
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"     
#include <AUnit.h>

const int motionPin = 04;
const int modePin = 16;

int pictureNumber = 0;

//Liverpool Internet
const char* ssid = "VM9090970_EXT";
const char* password = "nqr3tkftTcyx";


//Belfast Internet
/*
const char* ssid = "VM9880795";
const char* password = "ngSffjws5vrw";
*/

String serverName = "192.168.0.11";
String serverPath = "/securityDashboard/upload.php";
const int serverPort = 80;
WiFiClient client;
const char* ntpServer = "pool.ntp.org";
const long  gmtOffset_sec = 0;
const int   daylightOffset_sec = 3600;
int modeSelect;
boolean testFlag = false; //Change to TRUE to enter TEST MODE
const int sleepStartTime = 0;
const int sleepEndTime = 7;

#define EEPROM_SIZE 1
#define PART_BOUNDARY "123456789000000000000987654321"
#define uS_TO_S_FACTOR 1000000 //convertion factor(ms to s)

//Pin Definition for Board 
#define PWDN_GPIO_NUM 32
#define RESET_GPIO_NUM -1
#define XCLK_GPIO_NUM 0
#define SIOD_GPIO_NUM 26
#define SIOC_GPIO_NUM 27
#define Y9_GPIO_NUM 35
#define Y8_GPIO_NUM 34
#define Y7_GPIO_NUM 39
#define Y6_GPIO_NUM 36
#define Y5_GPIO_NUM 21
#define Y4_GPIO_NUM 19
#define Y3_GPIO_NUM 18
#define Y2_GPIO_NUM 5
#define VSYNC_GPIO_NUM 25
#define HREF_GPIO_NUM 23
#define PCLK_GPIO_NUM 22

boolean connectToWifi(){
   Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password); 
  int count = 0; 
  while (WiFi.status() != WL_CONNECTED) {
    if (count < 30){
    delay(250);
    Serial.print(".");
    count++;
    }
    else {
      Serial.print("WIFI Connection Failed - Exited Loop");
      return false;
    }
  }
  Serial.print(WiFi.status());
  if (WiFi.status() == WL_CONNECTED){
  Serial.print("ESP32-CAM IP Address: ");
  Serial.println(WiFi.localIP());
  return true;
  }
}

String uploadPhoto(){
  camera_fb_t * fb = NULL;
  fb = esp_camera_fb_get();
  if(!fb) {
    Serial.println("Camera capture failed");
    delay(1000);
    ESP.restart();
  }
  String getAll;
  String getBody;
    Serial.println("Connecting to server: " + serverName); 
    if (client.connect(serverName.c_str(), serverPort)) {
    Serial.println("Connection successful!");    
    String head = "--SecuritySystem\r\nContent-Disposition: form-data; name=\"imageFile\"; filename=\"esp32-cam.jpg\"\r\nContent-Type: image/jpeg\r\n\r\n";
    String tail = "\r\n--SecuritySystem--\r\n";

    uint32_t imageLen = fb->len;
    uint32_t extraLen = head.length() + tail.length();
    uint32_t totalLen = imageLen + extraLen;
  
    client.println("POST " + serverPath + " HTTP/1.1");
    client.println("Host: " + serverName);
    client.println("Content-Length: " + String(totalLen));
    client.println("Content-Type: multipart/form-data; boundary=SecuritySystem");
    client.println();
    client.print(head);
  
    uint8_t *fbBuf = fb->buf;
    size_t fbLen = fb->len;
    for (size_t n=0; n<fbLen; n=n+1024) {
      if (n+1024 < fbLen) {
        client.write(fbBuf, 1024);
        fbBuf += 1024;
      }
      else if (fbLen%1024>0) {
        size_t remainder = fbLen%1024;
        client.write(fbBuf, remainder);
      }
    }   
    client.print(tail);
    
    esp_camera_fb_return(fb);
    
    int timoutTimer = 10000;
    long startTimer = millis();
    boolean state = false;
    
    while ((startTimer + timoutTimer) > millis()) {
      Serial.print(".");
      delay(100);      
      while (client.available()) {
        char c = client.read();
        if (c == '\n') {
          if (getAll.length()==0) { state=true; }
          getAll = "";
        }
        else if (c != '\r') { getAll += String(c); }
        if (state==true) { getBody += String(c); }
        startTimer = millis();
      }
      if (getBody.length()>0) { break; }
    }
    Serial.println();
    client.stop();
    Serial.println(getBody);
    return "Photo Saved to Dashboard";
  }
}

String saveLocally(){
  camera_fb_t * fb = NULL;
  fb = esp_camera_fb_get();
  if(!fb) {
    Serial.println("Camera capture failed");
    delay(1000);
    ESP.restart();
  }
    if(!SD_MMC.begin()){
      String message = "SD Card Mount Failed"; 
      Serial.println(message);
      return message;
  }
  uint8_t cardType = SD_MMC.cardType();
  if(cardType == CARD_NONE){
    String message = "No SD Card attached"; 
    Serial.println(message);
    return message;
  }

  // initialize EEPROM with predefined size
  EEPROM.begin(EEPROM_SIZE);
  pictureNumber = EEPROM.read(0) + 1;

  // Path where new picture will be saved in SD Card
  String path = "/picture" + String(pictureNumber) +".jpg";

  fs::FS &fs = SD_MMC; 
  Serial.printf("Picture file name: %s\n", path.c_str());
  
  File file = fs.open(path.c_str(), FILE_WRITE);
  if(!file){
    Serial.println("Failed to open file in writing mode");
  } 
  else {
    file.write(fb->buf, fb->len); // payload (image), payload length
    Serial.printf("Saved file to path: %s\n", path.c_str());
    EEPROM.write(0, pictureNumber);
    EEPROM.commit();
  }
  file.close();
  esp_camera_fb_return(fb); 
  return "Photo Saved to Local Storage";
}

boolean cameraSetup(){
  camera_config_t config;
  config.ledc_channel = LEDC_CHANNEL_0;
  config.ledc_timer = LEDC_TIMER_0;
  config.pin_d0 = Y2_GPIO_NUM;
  config.pin_d1 = Y3_GPIO_NUM;
  config.pin_d2 = Y4_GPIO_NUM;
  config.pin_d3 = Y5_GPIO_NUM;
  config.pin_d4 = Y6_GPIO_NUM;
  config.pin_d5 = Y7_GPIO_NUM;
  config.pin_d6 = Y8_GPIO_NUM;
  config.pin_d7 = Y9_GPIO_NUM;
  config.pin_xclk = XCLK_GPIO_NUM;
  config.pin_pclk = PCLK_GPIO_NUM;
  config.pin_vsync = VSYNC_GPIO_NUM;
  config.pin_href = HREF_GPIO_NUM;
  config.pin_sscb_sda = SIOD_GPIO_NUM;
  config.pin_sscb_scl = SIOC_GPIO_NUM;
  config.pin_pwdn = PWDN_GPIO_NUM;
  config.pin_reset = RESET_GPIO_NUM;
  config.xclk_freq_hz = 20000000;
  config.pixel_format = PIXFORMAT_JPEG;

  if(psramFound()){
    config.frame_size = FRAMESIZE_UXGA;
    config.jpeg_quality = 10;
    config.fb_count = 2;
  }
  else{
    config.frame_size = FRAMESIZE_SVGA;
    config.jpeg_quality = 12;
    config.fb_count = 1;
  }

  //initalise camera
  esp_err_t err = esp_camera_init(&config);
  if (err != ESP_OK){
    Serial.printf("Camera initalisation failed with error 0x%x", err);
    delay(1000);
    return false;
  }
  Serial.println("Camera Init complete");
  return true;
}

httpd_handle_t stream_httpd = NULL;

static esp_err_t stream_handler(httpd_req_t *req){


static const char* _STREAM_CONTENT_TYPE = "multipart/x-mixed-replace;boundary=" PART_BOUNDARY;
static const char* _STREAM_BOUNDARY = "\r\n--" PART_BOUNDARY "\r\n";
static const char* _STREAM_PART = "Content-Type: image/jpeg\r\nContent-Length: %u\r\n\r\n";

  camera_fb_t * fb = NULL;
  esp_err_t res = ESP_OK;
  size_t _jpg_buf_len = 0;
  uint8_t * _jpg_buf = NULL;
  char * part_buf[64];

  res = httpd_resp_set_type(req, _STREAM_CONTENT_TYPE);
  if(res != ESP_OK){
    return res;
  }

  while(true){
    fb = esp_camera_fb_get();
    if (!fb) {
      Serial.println("Camera capture failed");
      res = ESP_FAIL;
    } else {
      if(fb->width > 400){
        if(fb->format != PIXFORMAT_JPEG){
          bool jpeg_converted = frame2jpg(fb, 80, &_jpg_buf, &_jpg_buf_len);
          esp_camera_fb_return(fb);
          fb = NULL;
          if(!jpeg_converted){
            Serial.println("JPEG compression failed");
            res = ESP_FAIL;
          }
        } else {
          _jpg_buf_len = fb->len;
          _jpg_buf = fb->buf;
        }
      }
    }
    if(res == ESP_OK){
      size_t hlen = snprintf((char *)part_buf, 64, _STREAM_PART, _jpg_buf_len);
      res = httpd_resp_send_chunk(req, (const char *)part_buf, hlen);
    }
    if(res == ESP_OK){
      res = httpd_resp_send_chunk(req, (const char *)_jpg_buf, _jpg_buf_len);
    }
    if(res == ESP_OK){
      res = httpd_resp_send_chunk(req, _STREAM_BOUNDARY, strlen(_STREAM_BOUNDARY));
    }
    if(fb){
      esp_camera_fb_return(fb);
      fb = NULL;
      _jpg_buf = NULL;
    } else if(_jpg_buf){
      free(_jpg_buf);
      _jpg_buf = NULL;
    }
    if(res != ESP_OK){
      break;
    }
  }
  return res;
}

void startCameraServer(){
  httpd_config_t config = HTTPD_DEFAULT_CONFIG();
  config.server_port = 80;

  httpd_uri_t index_uri = {
    .uri       = "/",
    .method    = HTTP_GET,
    .handler   = stream_handler,
    .user_ctx  = NULL
  };
  
  if (httpd_start(&stream_httpd, &config) == ESP_OK) {
    httpd_register_uri_handler(stream_httpd, &index_uri);
  }
}

std::string printLocalTime(){
  configTime(gmtOffset_sec, daylightOffset_sec, ntpServer);
  struct tm timeinfo;
   if(!getLocalTime(&timeinfo)){
    Serial.println("Failed to obtain time");
    return std::string("00:00:00");
  }
  char timeHour[3];
  strftime(timeHour,3, "%H", &timeinfo);
  return std::string(timeHour);
}
//Testing (Whitebox Tests)
String TimeTest(String ntpServer, int daylightOffset_sec){
    Serial.print("Testing Time [NTPServer = ");
    Serial.print(ntpServer);
    Serial.print(", DatLightSavOffset = ");
    Serial.print((String) daylightOffset_sec);
    Serial.print("]: ");
    Serial.println(("00:00:00" != printLocalTime()) ? "Pass" : "Fail");
}

//Testing (Unit Tests)
test(connectedToWiFi){
  assertEqual(connectToWifi(), true);
}
test(sendPhotoToServer){
  assertEqual(uploadPhoto(), (String) "Photo Saved to Dashboard");
}
test(sendPhotoToLocalStorage){
  assertEqual(saveLocally(), (String) "Photo Saved to Local Storage");
}

void setup() {
  // Serial port for debugging purposes
  WRITE_PERI_REG(RTC_CNTL_BROWN_OUT_REG, 0); //disable brownout detector
  Serial.setDebugOutput(false);
  Serial.begin(115200);
  pinMode(motionPin, INPUT);
  pinMode(modePin, INPUT);
  modeSelect = digitalRead(modePin);  
  if (testFlag == true){cameraSetup();}
  else if (connectToWifi()){ 
  std::string time = printLocalTime();
  int timeInt = atoi(time.c_str());
    if (timeInt > sleepStartTime && timeInt < sleepEndTime){     //sleep mode
      Serial.println("Power Saving Mode Enabled: within Sleep Times");
      // Check why system is awake
      esp_sleep_wakeup_cause_t wakeup_reason;
      wakeup_reason = esp_sleep_get_wakeup_cause();
      switch(wakeup_reason)
      {
          case ESP_SLEEP_WAKEUP_TIMER : 
            Serial.println("Wakeup caused by timer"); 
            if (timeInt < sleepEndTime){
              Serial.println("Back to Sleep: " + String(timeInt)); 
              esp_sleep_enable_timer_wakeup(3600 * uS_TO_S_FACTOR);
              esp_deep_sleep_start();
            }

            /* Note: Use this if you implement Dynamic Timer (Check time diff and reset sleep)
            int timeDiffInt = (sleepEndTime*3600) - (timeInt *3600); 
            esp_sleep_enable_timer_wakeup(timeDiffInt * uS_TO_S_FACTOR);
            */

            break;
          case ESP_SLEEP_WAKEUP_EXT0 : 
            Serial.println("Wake up cause by movement detected");
            cameraSetup();
            uploadPhoto();
            esp_sleep_enable_ext0_wakeup(GPIO_NUM_4,1);
            esp_deep_sleep_start();
            break;
          default : 
            Serial.printf("Wake up was not caused by deep sleep: %d\n",wakeup_reason); 
            break;
      }
    }

    else{
      Serial.println("Standard Mode Enabled: Starting Live View Server");
      cameraSetup();
      startCameraServer();
    }
}
else if (!connectToWifi() || modeSelect == HIGH){ //If it cannot connect to internet, safe mode enabled
  Serial.println("Safe Mode Enabled");
  cameraSetup();
  saveLocally();
  esp_sleep_enable_ext0_wakeup(GPIO_NUM_4,1);
  esp_deep_sleep_start(); 
}
}

void loop() {
  int motionDetected = digitalRead(motionPin);
  if (motionDetected > 0){
    Serial.println("Movement Detected - Photo taken and uploaded to Server : " + motionDetected);
    httpd_stop(stream_httpd);
    if (WiFi.status() == WL_CONNECTED){uploadPhoto();}
    else {saveLocally();}
    startCameraServer();
  }
  else if (testFlag == true){
    aunit::TestRunner::run();
  }
}